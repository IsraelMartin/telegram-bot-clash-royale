<?php
/**
 * (c) Israel Martín García <israel.martin.g@gmail.com>
 * See LICENSE.txt for license details.
 */

use App\Router;
use Controller\Add;
use Controller\AllNextChests;
use Controller\Chests;
use Controller\ChestsCallback;
use Controller\Help;
use Controller\Player;
use Controller\Remove;
use Controller\RemoveCallback;
use Controller\Start;
use Model\Keyboard\AllNextChests as AllNextChestsKeyboard;

Router::add('/^\/start$/i', Start::class);
Router::add('/^\/add$/i', Add::class);
Router::add('/^\/help$/i', Help::class);

Router::add('/^\/chests$/i', Chests::class);
Router::add('/^\/remove/i', Remove::class);

Router::add('/^chests_(?<playerTagId>\w+)$/i', ChestsCallback::class);
Router::add('/^remove_(?<playerTagId>\w+)$/i', RemoveCallback::class);

Router::add('/^#(?<playerTagId>.+)$/i', Player::class);

Router::add('/^' . AllNextChestsKeyboard::MESSAGE . '$/i', AllNextChests::class);
