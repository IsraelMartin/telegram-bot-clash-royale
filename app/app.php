<?php
/**
 * (c) Israel Martín García <israel.martin.g@gmail.com>
 * See LICENSE.txt for license details.
 */

use Api\AppInterface;
use App\Kernel;
use App\Request;
use Dice\Dice;

return new class() implements AppInterface {
	/** @var Dice */
	private $dice;

	/** @var Request */
	private $request;

	public function __construct()
	{
		$this->dice = new Dice();
		$this->registerRequest();
	}

	private function registerRequest(): void
	{
		$this->dice = $this->dice->addRule(Request::class,
			[
				'shared' => true,
			]
		);
	}

	public function request(): Request
	{
		/* @var Request $request */
		$this->request = $this->dice->create(Request::class);

		return $this->request;
	}

	public function kernel(): Kernel
	{
		/** @var Kernel $kernel */
		$kernel = $this->dice->create(Kernel::class);

		return $kernel;
	}
};
