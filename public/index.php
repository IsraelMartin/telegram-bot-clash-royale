<?php
/**
 * (c) Israel Martín García <israel.martin.g@gmail.com>
 * See LICENSE.txt for license details.
 */

use Api\AppInterface;
use Service\Logger;

\defined('ABSPATH') || \define('ABSPATH', \dirname(__DIR__));

require_once __DIR__ . '/../vendor/autoload.php';
require_once __DIR__ . '/../app/routes.php';

/** @var AppInterface $app */
$app = require_once __DIR__ . '/../app/app.php';

try {
	$app->kernel()->handle();
} catch (Throwable $exception) {
	(new Logger())->log([
		'exception' => $exception->getMessage(),
		'file' => $exception->getFile(),
		'line' => $exception->getLine(),
		]);
}

exit(0);
