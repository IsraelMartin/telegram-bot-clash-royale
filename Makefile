PHP_BIN = $(shell which php)

PHP_VERSION := $(shell $(PHP_BIN) --version | head -n 1 | cut -d " " -f 2 | cut -d "-" -f 1)
PHP_MAJOR_VERSION := $(shell $(PHP_BIN) --version | head -n 1 | cut -d " " -f 2 | cut -d "-" -f 1 | cut -c 1,1)

#PHPCSFIXER_BIN := php-cs-fixer
#PHPSTAN_BIN := phpstan

PHPCSFIXER_BIN := ./bin/php-cs-fixer
PHPSTAN_BIN := ./bin/phpstan
PHPUNIT_BIN := ./bin/phpunit
PHPMD_BIN := ./bin/phpmd
PHPSALM_BIN := ./bin/psalm
PHPMETRICS_BIN := ./bin/phpmetrics

#checking validators BIN
PHPCSFIXER_CHECK := $(shell command -v $(PHPCSFIXER_BIN) 2> /dev/null)
PHPSTAN_CHECK := $(shell command -v $(PHPSTAN_BIN) 2> /dev/null)

.PHONY: run
run:
	$(PHP_BIN) ./public/index.php

.PHONY: quality
quality: check-php-version fix analyze

.PHONY: quality-old
quality-old: cache-validators
ifeq (${PHP_MAJOR_VERSION}, 7)
	@echo "\n\033[92m***** COMPROBANDO ESTANDARES DE CALIDAD *****\033[0m"
ifndef PHPCSFIXER_CHECK
	$(error "php-cs-fixer is not available please install it with composer global install friendsofphp/php-cs-fixer")
endif
	-$(PHPCSFIXER_BIN) fix -v
ifndef PHPSTAN_CHECK
	$(error "phpstan is not available please install it with composer global install phpstan/phpstan")
endif
	-$(PHPSTAN_BIN) analyze --level=7 src/ tests/
else
	@echo "\n\033[92m***** LOS ESTANDARES DE CALIDAD DEBEN EJECUTARSE CON PHP >7.1 *****\033[0m"
endif

.PHONY: check-php-version
check-php-version:
ifeq (${PHP_MAJOR_VERSION}, 7)
	@echo "\n\033[92m***** COMPROBANDO ESTANDARES DE CALIDAD *****\033[0m"
else
	#@echo "\n\033[92m***** LOS ESTANDARES DE CALIDAD DEBEN EJECUTARSE CON PHP >7.1 *****\033[0m"
	$(error "***** LOS ESTANDARES DE CALIDAD DEBEN EJECUTARSE CON PHP >7.1 *****")
endif

.PHONY: fix
fix: cache-validators
ifndef PHPCSFIXER_CHECK
	$(error "php-cs-fixer is not available please install it with composer global install friendsofphp/php-cs-fixer")
endif
	-$(PHPCSFIXER_BIN) fix -v

.PHONY: analyze
analyze:
ifndef PHPSTAN_CHECK
	$(error "phpstan is not available please install it with composer global install phpstan/phpstan")
endif
	-$(PHPSTAN_BIN) analyze --level=7 -c phpstan.neon

.PHONY: cache-validators
cache-validators:
	$(RM) -r build .php_cs.cache

.PHONY: unit
unit:
	-$(PHPUNIT_BIN)

.PHONY: metrics
metrics:
	-$(PHPMETRICS_BIN) --report-html="./build/report" ./src

NGROK_URL := $(shell curl --silent --show-error http://127.0.0.1:4040/api/tunnels | sed -nE 's/.*public_url":"https:..([^"]*).*/\1/p')

.PHONY: btele
btele:
	curl --silent https://api.telegram.org/bot781202361:AAH59idtCiCBGUn59lKFYryYXTqYikm57X8/setWebhook?url=https://${NGROK_URL}
	@echo ""

.PHONY: mess
mess:
	-$(PHPMD_BIN) ./src/ text ruleset.xml

.PHONY: sal
sal:
	-$(PHPSALM_BIN) -c psalm.xml

.PHONY: list
list:
	@echo ""
	@echo "Opciones:"
	@echo "  run        > ejecuta el comando"
	@echo ""
	@echo "Desarrollo:"
	@echo "  fix        > Modifica los archivos PHP según el CS"
	@echo "  analyze    > Analiza el código PHP buscando errores con PHPStan"
	@echo "  quality    > Ejecuta fix y analyze"
	@echo "  unit       > Ejecuta test unitarios"
	@echo "  metrics    > Analiza el código PHP y genera informe de métricas"
	@echo "  mess       > Analiza el código PHP y muestra errores con PHPMess Detector"
	@echo "  sal        > Analiza el código PHP y muestra errores con PSALM"
	@echo "  btele      > Establece el webhook (de ngrok) de Telegram"
	@echo ""
