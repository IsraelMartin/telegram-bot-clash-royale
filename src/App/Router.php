<?php
/**
 * (c) Israel Martín García <israel.martin.g@gmail.com>
 * See LICENSE.txt for license details.
 */

namespace App;

use Controller\Fallback;

class Router
{
	/** @var Request */
	private $request;

	/** @var array */
	private static $routes = [];

	public function __construct(Request $request)
	{
		$this->request = $request;
	}

	public static function add(string $regexp, string $callback): void
	{
		self::$routes[] = [$regexp, $callback];
	}

	public function __invoke(): array
	{
		[$route, $matches] = $this->getRouteMatch();
		$params = \array_filter($matches, 'is_string', ARRAY_FILTER_USE_KEY);

		return [
			'route' => $route,
			'params' => $params,
		];
	}

	private function getRouteMatch(): array
	{
		$type = $this->request->getType();

		$data = ($type === $this->request::TYPE_CALLBACK)
			? $this->request->getCallback()
			: $this->request->getMessage();

		foreach (self::$routes as $route) {
			\preg_match($route[0], $data, $matches);
			if ([] !== $matches) {
				return [$route[1], $matches];
			}
		}

		return [Fallback::class, []];
	}
}
