<?php
/**
 * (c) Israel Martín García <israel.martin.g@gmail.com>
 * See LICENSE.txt for license details.
 */

namespace App;

use Exception;

class Environment
{
	/**
	 * @throws Exception
	 */
	public function getDatabaseHost(): string
	{
		$databaseHost = \getenv('DATABASE_HOST');
		if (false === $databaseHost) {
			throw new Exception('Environment variable DATABASE_HOST is not defined');
		}

		return $databaseHost;
	}

	/**
	 * @throws Exception
	 */
	public function getDatabaseUser(): string
	{
		$databaseUser = \getenv('DATABASE_USER');
		if (false === $databaseUser) {
			throw new Exception('Environment variable DATABASE_USER is not defined');
		}

		return $databaseUser;
	}

	/**
	 * @throws Exception
	 */
	public function getDatabasePass(): string
	{
		$databasePass = \getenv('DATABASE_PASS');
		if (false === $databasePass) {
			throw new Exception('Environment variable DATABASE_PASS is not defined');
		}

		return $databasePass;
	}

	/**
	 * @throws Exception
	 */
	public function getDatabaseName(): string
	{
		$databaseName = \getenv('DATABASE_NAME');
		if (false === $databaseName) {
			throw new Exception('Environment variable DATABASE_NAME is not defined');
		}

		return $databaseName;
	}

	/**
	 * @throws Exception
	 */
	public function getApiToken(): string
	{
		$apiToken = \getenv('API_TOKEN');
		if (false === $apiToken) {
			throw new Exception('Environment variable API_TOKEN is not defined');
		}

		return $apiToken;
	}
}
