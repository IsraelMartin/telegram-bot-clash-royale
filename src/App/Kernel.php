<?php
/**
 * (c) Israel Martín García <israel.martin.g@gmail.com>
 * See LICENSE.txt for license details.
 */

namespace App;

use Dice\Dice;
use Dotenv\Dotenv;
use Model\Telegram\TelegramManagement;
use Service\Logger;
use Service\SaveInteraction;

class Kernel
{
	/** @var Request */
	private $request;

	/** @var Router */
	private $router;

	/** @var Dice */
	private $dice;

	/** @var TelegramManagement */
	private $telegramManagement;

	/** @var Logger */
	private $logger;

	public function __construct(
		Request $request,
		Router $router,
		Dice $dice,
		TelegramManagement $telegramManagement,
		Logger $logger
	) {
		$this->request = $request;
		$this->router = $router;
		$this->dice = $dice;
		$this->telegramManagement = $telegramManagement;
		$this->logger = $logger;
	}

	public function handle(): void
	{
		$this->fetchEnvVariables();
		$this->logger->log($this->request->getRequest());
		// Middleware
		$response = $this->executeController();
		// Events
		$this->send($response);
	}

	private function fetchEnvVariables(): void
	{
		$dotenv = Dotenv::createImmutable(ABSPATH);
		$dotenv->load();
		$dotenv->required(['DATABASE_HOST', 'DATABASE_USER', 'DATABASE_PASS', 'DATABASE_NAME']);
		$dotenv->required(['API_URL', 'API_TOKEN']);
	}

	private function executeController(): Response
	{
		$command = $this->router->__invoke();
		$commandClass = $this->dice->create($command['route']);
		$interactionService = $this->dice->create(SaveInteraction::class);
		($interactionService)($command['route']);

		return ($commandClass)(...\array_values($command['params']));
	}

	private function send(Response $response): void
	{
		$url = \sprintf('%s/bot%s/sendMessage',
			\getenv('TELEGRAM_URL'),
			\getenv('TELEGRAM_TOKEN')
		);

		$keyboard = [
			'keyboard' => [
				[
					'💰 Próximos cofres',
				],
			],
			'resize_keyboard' => true,
			'one_time_keyboard' => true,
		];

		$replyMarkup = $keyboard;

		if ([] !== $response->getButtons()) {
			$inlineKeyBoard = [
				'inline_keyboard' => [
					$response->getButtons(),
				],
				'resize_keyboard' => true,
				'one_time_keyboard' => true,
			];
			$replyMarkup = $inlineKeyBoard;
		}

		$params = [
			'text' => $response->getMessage(),
			'chat_id' => $this->request->getChatId(),
			//'reply_markup' => ['force_reply' => true],
			'reply_markup' => \json_encode($replyMarkup),
			'parse_mode' => 'Markdown',
			//'reply_to_message_id' => $this->request->getMessageId(),
		];

		$this->telegramManagement->send($url, $params);
	}
}
