<?php
/**
 * (c) Israel Martín García <israel.martin.g@gmail.com>
 * See LICENSE.txt for license details.
 */

namespace App;

class Response
{
	/** @var string */
	private $message;

	/** @var array */
	private $inlineKeyboard = [];

	public function getMessage(): string
	{
		return $this->message;
	}

	public function setMessage(string $message): void
	{
		$this->message = $message;
	}

	public function getButtons(): array
	{
		return $this->inlineKeyboard;
	}

	public function addButton(string $text, string $callbackData): self
	{
		$this->inlineKeyboard[] = [
			'text' => $text,
			'callback_data' => $callbackData,
		];

		return $this;
	}
}
