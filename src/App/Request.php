<?php
/**
 * (c) Israel Martín García <israel.martin.g@gmail.com>
 * See LICENSE.txt for license details.
 */

namespace App;

use Api\RequestInterface;

class Request implements RequestInterface
{
	/** @var string */
	public const TYPE_CALLBACK = 'calback';

	/** @var string */
	public const TYPE_COMMAND = 'command';

	/** @var array */
	protected $request;

	public function __construct()
	{
		$this->request = \json_decode((string) \file_get_contents('php://input'), true);
	}

	public function getChatId(): ?string
	{
		return $this->request['message']['chat']['id'] ?? $this->request['callback_query']['message']['chat']['id'];
	}

	public function getMessageId(): string
	{
		return $this->request['message']['message_id'] ?? $this->request['callback_query']['message']['message_id'];
	}

	public function getName(): ?string
	{
		if (isset($this->request['message']['from']['first_name'])) {
			return (string) $this->request['message']['from']['first_name'];
		}

		return null;
	}

	public function getMessage(): ?string
	{
		if (isset($this->request['message']['text'])) {
			return (string) $this->request['message']['text'];
		}

		if (isset($this->request['edited_message']['text'])) {
			return (string) $this->request['edited_message']['text'];
		}

		return null;
	}

	public function getCallback(): ?string
	{
		if (isset($this->request['callback_query']['data'])) {
			return (string) $this->request['callback_query']['data'];
		}

		return null;
	}

	public function getCallbackMessage(): ?string
	{
		$callback = $this->getCallback();
		$callbackData = [];
		\preg_match('/^(chests|remove)_(\w+)$/i', $callback, $callbackData);

		return $callbackData[2];
	}

	public function getCallBackId(): ?string
	{
		if (isset($this->request['callback_query']['id'])) {
			return (string) $this->request['callback_query']['id'];
		}

		return null;
	}

	public function getUserId(): string
	{
		return $this->request['message']['from']['id'] ?? $this->request['callback_query']['from']['id'];
	}

	public function getType(): string
	{
		if (null !== $this->getCallback()) {
			return self::TYPE_CALLBACK;
		}

		return self::TYPE_COMMAND;
	}

	public function getRequest(): array
	{
		return $this->request;
	}
}
