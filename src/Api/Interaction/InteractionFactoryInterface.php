<?php
/**
 * (c) Israel Martín García <israel.martin.g@gmail.com>
 * See LICENSE.txt for license details.
 */

namespace Api\Interaction;

use Model\Interaction\Interaction;

interface InteractionFactoryInterface
{
	public function create(): Interaction;
}
