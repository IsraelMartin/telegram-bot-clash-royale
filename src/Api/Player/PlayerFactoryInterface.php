<?php
/**
 * (c) Israel Martín García <israel.martin.g@gmail.com>
 * See LICENSE.txt for license details.
 */

namespace Api\Player;

use Model\Player\Player;

interface PlayerFactoryInterface
{
	public function create(): Player;
}
