<?php
/**
 * (c) Israel Martín García <israel.martin.g@gmail.com>
 * See LICENSE.txt for license details.
 */

namespace Api;

use App\Kernel;
use App\Request;

interface AppInterface
{
	public function request(): Request;

	public function kernel(): Kernel;
}
