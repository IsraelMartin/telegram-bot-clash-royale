<?php
/**
 * (c) Israel Martín García <israel.martin.g@gmail.com>
 * See LICENSE.txt for license details.
 */

namespace Service;

use App\Request;
use Model\Interaction\InteractionFactory;
use Model\Interaction\InteractionRepository;

class SaveInteraction
{
	/** @var Request */
	private $request;

	/** @var InteractionFactory */
	private $interactionFactory;

	/** @var InteractionRepository */
	private $interactionRepository;

	public function __construct(
		Request $request,
		InteractionFactory $interactionFactory,
		InteractionRepository $interactionRepository
	) {
		$this->request = $request;
		$this->interactionFactory = $interactionFactory;
		$this->interactionRepository = $interactionRepository;
	}

	public function __invoke(string $controllerName): void
	{
		$interactionName = \explode('\\', $controllerName);

		$interaction = $this->interactionFactory->create();
		$interaction
			->setUserId($this->request->getUserId())
			->setCommand(\mb_strtolower(\end($interactionName)));

		$this->interactionRepository->save($interaction);
	}
}
