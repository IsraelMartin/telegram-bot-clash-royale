<?php
/**
 * (c) Israel Martín García <israel.martin.g@gmail.com>
 * See LICENSE.txt for license details.
 */

namespace Service;

use App\Response;

class ResponseFactory
{
	public function create(): Response
	{
		return new Response();
	}
}
