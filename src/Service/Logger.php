<?php
/**
 * (c) Israel Martín García <israel.martin.g@gmail.com>
 * See LICENSE.txt for license details.
 */

namespace Service;

class Logger
{
	/** @var string */
	private $logFile;

	public function __construct()
	{
		$this->logFile = '../var/requests.log';
	}

	public function log(array $data): void
	{
		if (null === $this->logFile) {
			return;
		}

		$fileHandler = \fopen($this->logFile, 'a+');
		if (\is_resource($fileHandler)) {
			\fwrite($fileHandler, \json_encode($data) . PHP_EOL);
			\fclose($fileHandler);
		}
	}
}
