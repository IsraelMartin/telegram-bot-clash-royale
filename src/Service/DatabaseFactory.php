<?php
/**
 * (c) Israel Martín García <israel.martin.g@gmail.com>
 * See LICENSE.txt for license details.
 */

namespace Service;

use App\Environment;
use Exception;
use mysqli;

class DatabaseFactory
{
	/** @var Environment */
	private $environment;

	public function __construct(Environment $environment)
	{
		$this->environment = $environment;
	}

	/**
	 * @throws Exception
	 */
	public function create(): mysqli
	{
		$mysqli = new mysqli(
			$this->environment->getDatabaseHost(),
			$this->environment->getDatabaseUser(),
			$this->environment->getDatabasePass(),
			$this->environment->getDatabaseName()
		);

		\mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);

		return $mysqli;
	}
}
