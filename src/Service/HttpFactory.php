<?php
/**
 * (c) Israel Martín García <israel.martin.g@gmail.com>
 * See LICENSE.txt for license details.
 */

namespace Service;

use App\Environment;
use Exception;
use GuzzleHttp\Client;

class HttpFactory
{
	/** @var Environment */
	private $environment;

	public function __construct(Environment $environment)
	{
		$this->environment = $environment;
	}

	/**
	 * @throws Exception
	 */
	public function create(): Client
	{
		return new Client([
			'base_uri' => \getenv('API_URL'),
			'timeot' => 2.0,
			'headers' => $this->headers(),
		]);
	}

	/**
	 * @throws Exception
	 */
	private function headers(): array
	{
		return [
			'Authorization' => 'Bearer ' . $this->environment->getApiToken(),
			'Accept' => 'application/json',
		];
	}
}
