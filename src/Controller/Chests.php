<?php
/**
 * (c) Israel Martín García <israel.martin.g@gmail.com>
 * See LICENSE.txt for license details.
 */

namespace Controller;

use Api\ControllerInterface;
use App\Request;
use App\Response;
use Model\Player\PlayerRepository;
use Service\ResponseFactory;

final class Chests implements ControllerInterface
{
	/** @var Request */
	private $request;

	/** @var Response */
	private $response;

	/** @var PlayerRepository */
	private $playerRepository;

	public function __construct(
		Request $request,
		ResponseFactory $responseFactory,
		PlayerRepository $playerRepository
	) {
		$this->request = $request;
		$this->response = $responseFactory->create();
		$this->playerRepository = $playerRepository;
	}

	public function __invoke(): Response
	{
		$userId = $this->request->getUserId();
		$players = $this->playerRepository->getList($userId);

		$responseMessage = 'Aún no has añadido ningún usuario';
		if ([] !== $players) {
			foreach ($players as $tagId => $player) {
				$this->response->addButton($player, 'chests_' . $tagId);
			}

			$responseMessage = '¿Qué usuario desea consultar?';
		}
		$this->response->setMessage($responseMessage);

		return $this->response;
	}
}
