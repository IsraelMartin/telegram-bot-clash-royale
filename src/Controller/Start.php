<?php
/**
 * (c) Israel Martín García <israel.martin.g@gmail.com>
 * See LICENSE.txt for license details.
 */

namespace Controller;

use Api\ControllerInterface;
use App\Response;
use Service\ResponseFactory;

final class Start implements ControllerInterface
{
	/** @var Response */
	private $response;

	public function __construct(ResponseFactory $responseFactory)
	{
		$this->response = $responseFactory->create();
	}

	public function __invoke(): Response
	{
		$message = <<< EOD
*Bienvenido a MisCofresCR*

Aquí puedes realizar diversas consultas sobre tu cuenta de Clash Royale
¿Quieres saber qué cofres te van a salir?
Añade tu usuario de Clash Royale y podrás consultarlo
EOD;

		$this->response->setMessage($message);

		return $this->response;
	}
}
