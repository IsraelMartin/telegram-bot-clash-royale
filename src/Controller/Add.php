<?php
/**
 * (c) Israel Martín García <israel.martin.g@gmail.com>
 * See LICENSE.txt for license details.
 */

namespace Controller;

use Api\ControllerInterface;
use App\Response;
use Service\ResponseFactory;

final class Add implements ControllerInterface
{
	/** @var Response */
	private $response;

	public function __construct(ResponseFactory $responseFactory)
	{
		$this->response = $responseFactory->create();
	}

	public function __invoke(): Response
	{
		$message = <<<EOD
Escribe el ID del usuario en Clash Royale (empieza por "#")
Este ID aparece en tu perfil de usuario debajo del nombre.
EOD;

		$this->response->setMessage($message);

		return $this->response;
	}
}
