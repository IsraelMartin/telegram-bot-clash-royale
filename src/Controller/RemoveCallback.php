<?php
/**
 * (c) Israel Martín García <israel.martin.g@gmail.com>
 * See LICENSE.txt for license details.
 */

namespace Controller;

use Api\ControllerInterface;
use App\Request;
use App\Response;
use Exception;
use Model\Player\PlayerRepository;
use Service\ResponseFactory;

final class RemoveCallback implements ControllerInterface
{
	/** @var Request */
	private $request;

	/** @var Response */
	private $response;

	/** @var PlayerRepository */
	private $playerRepository;

	public function __construct(
		Request $request,
		ResponseFactory $responseFactory,
		PlayerRepository $playerRepository
	) {
		$this->request = $request;
		$this->response = $responseFactory->create();
		$this->playerRepository = $playerRepository;
	}

	/**
	 * @throws Exception
	 *
	 * @param string $playerTagId
	 */
	public function __invoke(string $playerTagId): Response
	{
		$player = $this->playerRepository->get($playerTagId, $this->request->getUserId());

		try {
			$this->playerRepository->delete($player);
			$responseMessage = 'Usuario eliminado';
		} catch (Exception $exception) {
			$responseMessage = 'El usuario no se ha podido eliminar';
		}

		$this->response->setMessage($responseMessage);

		return $this->response;
	}
}
