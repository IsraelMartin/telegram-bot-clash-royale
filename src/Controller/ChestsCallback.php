<?php
/**
 * (c) Israel Martín García <israel.martin.g@gmail.com>
 * See LICENSE.txt for license details.
 */

namespace Controller;

use Api\ControllerInterface;
use App\Request;
use App\Response;
use Exception;
use Model\Chest\ChestRepository;
use Model\Player\PlayerRepository;
use Service\ResponseFactory;

final class ChestsCallback implements ControllerInterface
{
	/** @var Request */
	private $request;

	/** @var Response */
	private $response;

	/** @var ChestRepository */
	private $chestRepository;

	/** @var PlayerRepository */
	private $playerRepository;

	public function __construct(
		Request $request,
		ResponseFactory $responseFactory,
		ChestRepository $chestRepository,
		PlayerRepository $playerRepository
	) {
		$this->request = $request;
		$this->response = $responseFactory->create();
		$this->chestRepository = $chestRepository;
		$this->playerRepository = $playerRepository;
	}

	/**
	 * @throws Exception
	 */
	public function __invoke(): Response
	{
		$playerId = $this->request->getCallbackMessage();
		$cofres = $this->chestRepository->get($playerId);
		$player = $this->playerRepository->get($playerId, $this->request->getUserId());

		$playerName = $player->getClashRoyaleName();
		$cofresText = \implode(PHP_EOL, $cofres);
		$message = <<< EOD
Próximos cofres de *{$playerName}*:
$cofresText
EOD;

		$this->response->setMessage($message);

		return $this->response;
	}
}
