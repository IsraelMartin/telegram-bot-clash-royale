<?php
/**
 * (c) Israel Martín García <israel.martin.g@gmail.com>
 * See LICENSE.txt for license details.
 */

namespace Controller;

use Api\ControllerInterface;
use App\Response;
use Service\ResponseFactory;

final class Fallback implements ControllerInterface
{
	/** @var ResponseFactory */
	private $responseFactory;

	public function __construct(ResponseFactory $responseFactory)
	{
		$this->responseFactory = $responseFactory;
	}

	public function __invoke(): Response
	{
		$response = $this->responseFactory->create();

		$message = <<< EOD
No se ha encontrado el comando.
Para ver los comandos disponibles escribe "/" y aparecerán las opciones.
EOD;
		$response->setMessage($message);

		return $response;
	}
}
