<?php
/**
 * (c) Israel Martín García <israel.martin.g@gmail.com>
 * See LICENSE.txt for license details.
 */

namespace Controller;

use Api\ControllerInterface;
use App\Response;
use Service\ResponseFactory;

final class Help implements ControllerInterface
{
	/** @var Response */
	private $response;

	public function __construct(ResponseFactory $responseFactory)
	{
		$this->response = $responseFactory->create();
	}

	public function __invoke(): Response
	{
		$message = <<< EOD
*Bot personal de Israel MG*

Para contactar escribir a israel.martin.g@gmail.com
EOD;

		$this->response->setMessage($message);

		return $this->response;
	}
}
