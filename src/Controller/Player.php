<?php
/**
 * (c) Israel Martín García <israel.martin.g@gmail.com>
 * See LICENSE.txt for license details.
 */

namespace Controller;

use Api\ControllerInterface;
use App\Request;
use App\Response;
use Model\Player\PlayerFactory;
use Model\Player\PlayerRepository;
use Model\PlayerApi\PlayerApiRepository;
use Service\ResponseFactory;

final class Player implements ControllerInterface
{
	/** @var Request */
	private $request;

	/** @var Response */
	private $response;

	/** @var PlayerApiRepository */
	private $playerApiRepository;

	/** @var PlayerFactory */
	private $playerFactory;

	/** @var PlayerRepository */
	private $playerRepository;

	public function __construct(
		Request $request,
		ResponseFactory $responseFactory,
		PlayerApiRepository $playerApiRepository,
		PlayerFactory $playerFactory,
		PlayerRepository $playerRepository
	) {
		$this->request = $request;
		$this->response = $responseFactory->create();
		$this->playerApiRepository = $playerApiRepository;
		$this->playerFactory = $playerFactory;
		$this->playerRepository = $playerRepository;
	}

	public function __invoke(): Response
	{
		$data = $this->request->getMessage();
		$dataFiltered = [];
		\preg_match('/^#?(.+)$/i', $data, $dataFiltered);

		$playerTagId = $dataFiltered[1];
		try {
			$nombreUsuario = $this->playerApiRepository->get($playerTagId);
			$player = $this->playerFactory->create();
			$player
				->setUserId($this->request->getUserId())
				->setTagId($playerTagId)
				->setClashRoyaleName($nombreUsuario);
			$this->playerRepository->save($player);
			$this->response->setMessage('Usuario ' . $nombreUsuario . ' añadido');
		} catch (\Exception $exception) {
			$this->response->setMessage('Usuario no encontrado');
		}

		return $this->response;
	}
}
