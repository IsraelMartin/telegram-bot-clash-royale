<?php
/**
 * (c) Israel Martín García <israel.martin.g@gmail.com>
 * See LICENSE.txt for license details.
 */

namespace Controller;

use Api\ControllerInterface;
use App\Request;
use App\Response;
use Model\Chest\ChestRepository;
use Model\Player\PlayerRepository;
use Service\ResponseFactory;

final class AllNextChests implements ControllerInterface
{
	/** @var Request */
	private $request;

	/** @var ResponseFactory */
	private $responseFactory;

	/** @var PlayerRepository */
	private $playerRepository;

	/** @var ChestRepository */
	private $chestRepository;

	public function __construct(
		Request $request,
		ResponseFactory $responseFactory,
		PlayerRepository $playerRepository,
		ChestRepository $chestRepository
	) {
		$this->request = $request;
		$this->responseFactory = $responseFactory;
		$this->playerRepository = $playerRepository;
		$this->chestRepository = $chestRepository;
	}

	/**
	 * @throws \Exception
	 */
	public function __invoke(): Response
	{
		$response = $this->responseFactory->create();

		$userId = $this->request->getUserId();
		$players = $this->playerRepository->getList($userId);

		$responseMessage = 'Aún no has añadido ningún usuario';
		if ([] !== $players) {
			$responseMessage = '';
			foreach ($players as $tagId => $player) {
				$cofres = $this->chestRepository->get((string) $tagId);
				$player = $this->playerRepository->get((string) $tagId, $this->request->getUserId());
				$playerName = $player->getClashRoyaleName();
				$cofresText = \implode(PHP_EOL, $cofres);
				$responseMessage .= <<< EOD
Próximos cofres de *{$playerName}*:
$cofresText


EOD;
			}
		}

		$response->setMessage($responseMessage);

		return $response;
	}
}
