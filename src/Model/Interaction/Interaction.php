<?php
/**
 * (c) Israel Martín García <israel.martin.g@gmail.com>
 * See LICENSE.txt for license details.
 */

namespace Model\Interaction;

final class Interaction
{
	/** @var string */
	private $userId;

	/** @var string */
	private $command;

	public function getUserId(): string
	{
		return $this->userId;
	}

	public function setUserId(string $userId): self
	{
		$this->userId = $userId;

		return $this;
	}

	public function getCommand(): string
	{
		return $this->command;
	}

	public function setCommand(string $command): self
	{
		$this->command = $command;

		return $this;
	}
}
