<?php
/**
 * (c) Israel Martín García <israel.martin.g@gmail.com>
 * See LICENSE.txt for license details.
 */

namespace Model\Interaction;

use Api\Interaction\InteractionFactoryInterface;

final class InteractionFactory implements InteractionFactoryInterface
{
	public function create(): Interaction
	{
		return new Interaction();
	}
}
