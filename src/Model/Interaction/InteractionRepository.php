<?php
/**
 * (c) Israel Martín García <israel.martin.g@gmail.com>
 * See LICENSE.txt for license details.
 */

namespace Model\Interaction;

use Service\DatabaseFactory;

final class InteractionRepository
{
	/** @var DatabaseFactory */
	private $databaseFactory;

	public function __construct(DatabaseFactory $databaseFactory)
	{
		$this->databaseFactory = $databaseFactory;
	}

	public function save(Interaction $interaction): void
	{
		$database = $this->databaseFactory->create();
		$query = \sprintf(
			'insert into interactions (userId, command) values ("%s", "%s")',
			$interaction->getUserId(),
			$interaction->getCommand()
		);
		$database->query($query);
	}
}
