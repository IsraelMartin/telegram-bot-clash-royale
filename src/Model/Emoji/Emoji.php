<?php
/**
 * (c) Israel Martín García <israel.martin.g@gmail.com>
 * See LICENSE.txt for license details.
 */

namespace Model\Emoji;

interface Emoji
{
	public const MONEY_BAG = "\xF0\x9F\x92\xB0";

	public const CHEST_SILVER = "\xF0\x9F\xA5\x88";

	public const CHEST_GOLDEN = "\xF0\x9F\xA5\x87";

	public const CHEST_MEGA_LIGHTNING = "\xE2\x9A\xA1";

	public const CHEST_MAGICAL = "\xF0\x9F\x94\xAE";

	public const CHEST_LEGENDARY = "\xF0\x9F\x92\x8E";

	public const CHEST_EPIC = "\xF0\x9F\x90\xB2";

	public const CHEST_GIANT = "\xF0\x9F\x90\xB3";

	public const CHEST_GOLD_CRATE_OVERFLOWING = "\xF0\x9F\x92\xB6";

	public const CHEST_GOLD_CRATE_PLENTIFUL = "\xF0\x9F\x92\xB7";

	public const CHEST_GOLD_CRATE = "\xF0\x9F\x92\xB4";

	public const CHEST_ROYAL_WILD = "\xF0\x9F\x83\x8F";
}
