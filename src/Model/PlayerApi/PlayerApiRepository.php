<?php
/**
 * (c) Israel Martín García <israel.martin.g@gmail.com>
 * See LICENSE.txt for license details.
 */

namespace Model\PlayerApi;

use Exception;
use GuzzleHttp\Client;
use Service\HttpFactory;

final class PlayerApiRepository
{
	/** @var Client */
	private $httpclient;

	public function __construct(HttpFactory $httpFactory)
	{
		$this->httpclient = $httpFactory->create();
	}

	/**
	 * @throws Exception
	 *
	 * @param string $playerTagId
	 */
	public function get(string $playerTagId): string
	{
		$url = \sprintf('players/%%23%s', $playerTagId);

		$res = $this->httpclient->request('GET', $url);
		if (200 === $res->getStatusCode()) {
			$value = \json_decode($res->getBody()->getContents());

			return $value->name;
		}

		throw new Exception('Usuario no encontrado');
	}
}
