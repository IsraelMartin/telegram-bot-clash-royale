<?php
/**
 * (c) Israel Martín García <israel.martin.g@gmail.com>
 * See LICENSE.txt for license details.
 */

namespace Model\Chest;

use Exception;
use Model\Emoji\Emoji;
use Service\HttpFactory;

final class ChestRepository
{
	/** @var HttpFactory */
	private $client;

	public function __construct(HttpFactory $client)
	{
		$this->client = $client;
	}

	public function get(string $playerId): array
	{
		$requestClient = $this->client->create();
		$url = \sprintf('players/%%23%s/upcomingchests',
			\strtoupper($playerId)
		);

		try {
			$res = $requestClient->request('GET', $url);
		} catch (Exception $exception) {
			return [
				'No se han encontrado datos.',
				'Inténtelo de nuevo.',
			];
		}

		if (isset($res) && 200 === $res->getStatusCode()) {
			$value = \json_decode($res->getBody()->getContents());
			if (!isset($value->items)) {
				return [
					'No se han encontrado datos.',
					'Inténtelo de nuevo.',
				];
			}

			$message = [];
			$proximos = $value->items;
			foreach ($proximos as $proximo) {
				$proximoName = $this->getChestName($proximo->name);
				$nextCount = (string) ($proximo->index + 1);
				$message[] = \str_pad($nextCount, 3, '0', STR_PAD_LEFT) . ' - ' . $proximoName;
			}

			return $message;
		}

		return [
			'No se han encontrado datos.',
			'Inténtelo de nuevo.',
		];
	}

	private function getChestName(string $originalChestName): string
	{
		$chestName = $originalChestName;

		$cofresName = [
			'Silver Chest' => Emoji::CHEST_SILVER . 'Plata (3h)',
			'Golden Chest' => Emoji::CHEST_GOLDEN . 'Oro (8h)',
			'Mega Lightning Chest' => Emoji::CHEST_MEGA_LIGHTNING . 'Mega Relámpago (24h)',
			'Magical Chest' => Emoji::CHEST_MAGICAL . 'Mágico (12h)',
			'Legendary Chest' => Emoji::CHEST_LEGENDARY . 'Legendario (24h)',
			'Epic Chest' => Emoji::CHEST_EPIC . 'Épico (12h)',
			'Giant Chest' => Emoji::CHEST_GIANT . 'Gigante (12h)',
			'Overflowing Gold Crate' => Emoji::CHEST_GOLD_CRATE_OVERFLOWING . ' Caja de oro rebosante (12h)',
			'Plentiful Gold Crate' => Emoji::CHEST_GOLD_CRATE_PLENTIFUL . ' Caja de oro abundante (8h)',
			'Gold Crate' => Emoji::CHEST_GOLD_CRATE . ' Caja de oro (3h)',
			'Royal Wild Chest' => Emoji::CHEST_ROYAL_WILD . 'Cofre de comodines real (24h)',
		];

		if (\array_key_exists($originalChestName, $cofresName)) {
			$chestName = $cofresName[$originalChestName];
		}

		return $chestName;
	}
}
