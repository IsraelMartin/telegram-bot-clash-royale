<?php
/**
 * (c) Israel Martín García <israel.martin.g@gmail.com>
 * See LICENSE.txt for license details.
 */

namespace Model\Keyboard;

class AllNextChests
{
	/** @var string */
	public const MESSAGE = '💰 Próximos cofres';
}
