<?php
/**
 * (c) Israel Martín García <israel.martin.g@gmail.com>
 * See LICENSE.txt for license details.
 */

namespace Model\Player;

final class Player
{
	/** @var string */
	private $userId;

	/** @var string */
	private $tagId;

	/** @var string */
	private $clashRoyaleName;

	public function setUserId(string $userId): self
	{
		$this->userId = $userId;

		return $this;
	}

	public function getUserId(): string
	{
		return $this->userId;
	}

	public function setTagId(string $tagId): self
	{
		$this->tagId = $tagId;

		return $this;
	}

	public function getTagId(): string
	{
		return $this->tagId;
	}

	public function setClashRoyaleName(string $clashRoyaleName): self
	{
		$this->clashRoyaleName = $clashRoyaleName;

		return $this;
	}

	public function getClashRoyaleName(): string
	{
		return $this->clashRoyaleName;
	}
}
