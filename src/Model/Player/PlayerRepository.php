<?php
/**
 * (c) Israel Martín García <israel.martin.g@gmail.com>
 * See LICENSE.txt for license details.
 */

namespace Model\Player;

use Exception;
use Service\DatabaseFactory;

final class PlayerRepository
{
	/**
	 * @var DatabaseFactory
	 */
	private $databaseFactory;

	/**
	 * @var PlayerFactory
	 */
	private $playerFactory;

	public function __construct(DatabaseFactory $databaseFactory, PlayerFactory $playerFactory)
	{
		$this->databaseFactory = $databaseFactory;
		$this->playerFactory = $playerFactory;
	}

	/**
	 * @throws Exception
	 *
	 * @param string $tagId
	 * @param string $userId
	 */
	public function get(string $tagId, string $userId): Player
	{
		$database = $this->databaseFactory->create();

		$query = \sprintf('select * from users where userId = "%s" and tagId="%s" limit 1',
			$userId,
			$tagId
		);

		$user = $database->query($query);

		$userData = $user->fetch_array(MYSQLI_ASSOC);

		if (!\array_key_exists('tagId', $userData) ||
			!\array_key_exists('userId', $userData) ||
			!\array_key_exists('clashroyaleName', $userData)
		) {
			throw new Exception('Usuario no encontrado');
		}

		$player = $this->playerFactory->create();
		$player
			->setTagId($userData['tagId'])
			->setUserId($userData['userId'])
			->setClashRoyaleName($userData['clashroyaleName']);

		return $player;
	}

	/**
	 * @throws Exception
	 *
	 * @param string $userId
	 */
	public function getList(string $userId): array
	{
		$database = $this->databaseFactory->create();

		$clashroyalesUsers = $database->query("SELECT * FROM users WHERE userId = \"$userId\" limit 4");

		$usuarios = [];

		while ($myrow = $clashroyalesUsers->fetch_array(MYSQLI_ASSOC)) {
			$usuarios[$myrow['tagId']] = $myrow['clashroyaleName'];
		}

		return $usuarios;
	}

	/**
	 * @throws Exception
	 *
	 * @param Player $player
	 */
	public function save(Player $player): void
	{
		$database = $this->databaseFactory->create();
		$query = \sprintf(
			'insert into users (userId, tagId, clashroyaleName) values ("%s", "%s", "%s");',
			$player->getUserId(),
			$player->getTagId(),
			$player->getClashRoyaleName()
		);

		if (!$database->query($query)) {
			throw new Exception('Error al añadir el usuario');
		}
	}

	/**
	 * @throws Exception
	 *
	 * @param Player $player
	 */
	public function delete(Player $player): void
	{
		$database = $this->databaseFactory->create();

		$query = \sprintf('DELETE FROM users WHERE userId = "%s" AND tagId ="%s"',
			$player->getUserId(),
			$player->getTagId()
		);

		if (!$database->query($query)) {
			throw new Exception('Fallo al borrar el usuario');
		}
	}
}
