<?php
/**
 * (c) Israel Martín García <israel.martin.g@gmail.com>
 * See LICENSE.txt for license details.
 */

namespace Model\Player;

use Api\Player\PlayerFactoryInterface;

final class PlayerFactory implements PlayerFactoryInterface
{
	public function create(): Player
	{
		return new Player();
	}
}
