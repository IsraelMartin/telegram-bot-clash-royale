<?php
/**
 * (c) Israel Martín García <israel.martin.g@gmail.com>
 * See LICENSE.txt for license details.
 */

namespace Model\Telegram;

class TelegramMetadata
{
	public const PARSE_MODE = 'parse_mode';
}
