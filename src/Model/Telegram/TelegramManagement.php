<?php
/**
 * (c) Israel Martín García <israel.martin.g@gmail.com>
 * See LICENSE.txt for license details.
 */

namespace Model\Telegram;

use Service\HttpFactory;

class TelegramManagement
{
	/** @var HttpFactory */
	private $httpFactory;

	public function __construct(HttpFactory $httpFactory)
	{
		$this->httpFactory = $httpFactory;
	}

	public function send(string $url, array $parameters): void
	{
		$httpClient = $this->httpFactory->create();
		$url .= '?' . \http_build_query($parameters);
		$httpClient->request('GET', $url);
	}
}
