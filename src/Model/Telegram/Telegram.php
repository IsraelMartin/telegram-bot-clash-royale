<?php
/**
 * (c) Israel Martín García <israel.martin.g@gmail.com>
 * See LICENSE.txt for license details.
 */

namespace Model\Telegram;

class Telegram
{
	public function getParameters(): array
	{
		return [
			'reply_markup' => null,
			'parse_mode' => 'Markdown',
		];
	}
}
